import React              from 'react'
import ReactDOM           from 'react-dom'
import regeneratorRuntime from 'regenerator-runtime'

function App() {
	const [ state, setState ] = React.useState( {
		data:        [],
		activePanel: 0,
	} )

	React.useEffect( () => {
		const fetchData = async () => {
			fetch( 'https://next.tnw-staging.com/next-api/tickets.json' )
				.then( response => response.json() )
				.then( response => {
					setState( {
						...response,
						activePanel: 0,
					} )
				} )
		}

		fetchData()
	}, [] )

	if ( ! state.data.length ) {
		return null
	}

	return (
		<React.Fragment>
			<ul className="tabs">
				{
					state.data.map( ( panel, key ) => (
						<li className="tab" key={ key }>
							<button className={ 'toggle-panel' + ( state.activePanel === key ? ' active' : '' ) } href={ '#panel-' + key } onClick={ () => setState( {
								...state,
								activePanel: key,
							} ) }>{ panel.title }</button>
						</li>
					) )
				}
			</ul>

			<div className="panels">
				{
					state.data.map( ( panel, key ) => {
						var perks = []

						panel.tickets.forEach( ( ticket ) => {
							ticket.ticketPerks.forEach( ( perk ) => {
								// ignore inactive perks
								if ( '1' !== perk.perkActive ) {
									return true
								}

								// avoid duplicates
								if ( -1 !== perks.indexOf( perk.col1 ) ) {
									return true
								}

								perks.push( perk.col1 )
							} )
						} )

						return (
							<div className={ 'pricing-table' + ( state.activePanel === key ? ' active' : '' ) } id={ 'panel-' + key } key={ key }>
								<div className="pricing-table__ticket hidden-mobile" aria-hidden="true">
									<div className="pricing-table__ticket-header"></div>

									<ul className="pricing-table__ticket-perks">
										{
											perks.map( ( perk, key ) => (
												<li className="pricing-table__ticket-perk" key={ key }>{ perk }</li>
											) )
										}
									</ul>
								</div>

								{
									panel.tickets.map( ( ticket, key ) => (
										<div className={ 'pricing-table__ticket' + ( ticket.ticketHighlighted ? ' pricing-table__ticket-highlighted' : '' ) } key={ key }>
											<div className="pricing-table__ticket-header">
												<h2 className="pricing-table__ticket-title">{ ticket.ticketName }</h2>

												<div className="pricing-table__ticket-top-label">
													{ ticket.ticketTopLabel ? ( <div className="pricing-table__ticket-flash">{ ticket.ticketTopLabel }</div> ) : null }
												</div>

												<p className="pricing-table__ticket-description">{ ticket.ticketDescription }</p>

												<div className="pricing-table__ticket-price">
													<div className="pricing-table__ticket-price-amount">
														{ ticket.ticketStrikethroughPrice ? ( <strike className="pricing-table__ticket-price-full">&euro; { ticket.ticketStrikethroughPrice }</strike> ) : null }

														<ins className="pricing-table__ticket-price-sale">&euro; { ticket.ticketActualPrice }</ins>
													</div>

													<small className="pricing-table__ticket-price-fineprint">ex. 21% VAT</small>
												</div>

												<div className="pricing-table__ticket-buy">
													<a className="pricing-table__ticket-button" href={ ticket.ticketButtonLink }>{ ticket.ticketButtonLabel }</a>
												</div>
											</div>

											<ul className="pricing-table__ticket-perks">
												{
													perks.map( function( perk, key ) {
														var icon = <i className="pricing-table__ticket-perk-icon pricing-table__ticket-perk-icon-not-available hidden-mobile" aria-hidden="true"></i>,
															text = <span className="pricing-table__ticket-perk-label visibility-hidden" aria-hidden="true">{ perk }</span>

														ticket.ticketPerks.map( function( ticketPerk, key ) {
															if ( ticketPerk.col1 == perk ) {
																icon = <i className="pricing-table__ticket-perk-icon pricing-table__ticket-perk-icon-available hidden-mobile" aria-hidden="true"></i>
																text = <span className="pricing-table__ticket-perk-label visibility-hidden-desktop">{ perk }</span>

																return false
															}
														} )

														return (
															<li className="pricing-table__ticket-perk" key={ key }>
																{ icon }
																{ text }
															</li>
														)
													} )
												}
											</ul>

											<div className="pricing-table__ticket-footer">
												<div className="pricing-table__ticket-price">
													<div className="pricing-table__ticket-price-amount">
														{ ticket.ticketStrikethroughPrice ? ( <strike className="pricing-table__ticket-price-full">&euro; { ticket.ticketStrikethroughPrice }</strike> ) : null }

														<ins className="pricing-table__ticket-price-sale">&euro; { ticket.ticketActualPrice }</ins>
													</div>

													<small className="pricing-table__ticket-price-fineprint">ex. 21% VAT</small>
												</div>

												<div className="pricing-table__ticket-buy">
													<a className="pricing-table__ticket-button" href={ ticket.ticketButtonLink }>{ ticket.ticketButtonLabel }</a>
												</div>
											</div>
										</div>
									) )
								}
							</div>
						)
					} )
				}
			</div>
		</React.Fragment>
	)
}

ReactDOM.render(
	<App />,
	document.getElementById( 'root' )
)
