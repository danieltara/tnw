const webpack = require( 'webpack' );
const path    = require( 'path' );
const minicss = require( 'mini-css-extract-plugin' );
const terser  = require( 'terser-webpack-plugin' );

module.exports = {
	mode: 'development',
	resolve: {
		fallback: {
			'path': require.resolve( 'path-browserify' ),
		},
	},
	entry: {
		app:   path.resolve( 'src', 'jsx/app.jsx' ),
		style: path.resolve( 'src', 'scss/style.scss' ),
	},
	output: {
		path:     path.resolve( __dirname, 'dist' ),
		filename: 'js/[name].js',
	},
	plugins: [
		new minicss({
			filename: 'css/[name].css',
		})
	],
	module: {
		rules: [
			{
				test:    /\.scss$/,
				exclude: /node_modules/,
				use:     [
					minicss.loader,
					'css-loader',
					'sass-loader',
				],
			},
			{
				test:    /\.jsx?$/,
				exclude: /node_modules/,
				use:     'babel-loader',
			},
			{
				test:    /\.(woff2?|ttf|svg)$/i,
				include: path.resolve( __dirname, 'src/fonts' ),
				use: [{
					loader:   'file-loader',
					options:  {
						name: '[name].[ext]',
						outputPath: 'fonts/',
						publicPath: '../fonts/',
					},
				}],
			},
		],
	},
	optimization: {
		minimizer: [ new terser( {
			extractComments: false,
		} ) ],
	},
}
